from flask import Flask, render_template, flash, request
from wtforms import Form, TextField, TextAreaField, validators, StringField, SubmitField, BooleanField
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate

# App config.
DEBUG = True
app = Flask(__name__)
app.config.from_object(__name__)
app.config['SECRET_KEY'] = '7d441f27d441f27567d441f2b6176a'
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://fshmuser:bjpdowndown@localhost:5432/fshmcampaign'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

#database intialization
db = SQLAlchemy(app)
db.init_app(app)
migrate = Migrate(app, db)

#models are here
class Signature(db.Model):
    __tablename__ = 'signature_campaign'

    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String())
    email = db.Column(db.String())

    def __init__(self, name, email):
        self.name = name
        self.email = email

    def __repr__(self):
        return f"{self.name}:{self.email}"



class ReusableForm(Form):
    name = TextField('Name', validators=[validators.DataRequired(),validators.Length(min=3, max=50)])
    email = TextField('Email', validators=[validators.DataRequired(), validators.Length(min=6, max=50)])
    address = TextAreaField('Address', validators=[validators.DataRequired(), validators.Length(min=6, max=250)])
    got_sms_call = BooleanField('I Agree that I receive either call or SMS from BJP', false_values=(False))
    bjp_collected_data = BooleanField('I agree that I did not handover my mobile number to any of the personel from BJP', false_values=(False))
    bjp_missed_call_campaign = BooleanField('I agree that I did not participate in any of the signature campaign organised by BJP', false_values=(False))
    constituency = SelectField('Constituency', choices=[
        ('thirubuvanai', 'Thirubuvanai'),
        ('indiranagar', 'Indira Nagar'), 
        ('nedungadu', 'Nedungadu'),
        ('thattanchavady', 'Thattanchavady'),
        ('mannadipet', 'Mannadipet'),
        ('ossudu', 'Ossudu'),
        ('mangalam', 'Mangalam'),
        ('villianur', 'Villianur'),
        ('ozhukarai', 'Ozhukarai'),
        ('kadirkamam', 'Kadirkamam'),
        ('kamarajnagar', 'Kamaraj Nagar'),
        ('lawspet', 'Lawspet'),
        ('kalapet', 'Kalapet'),
        ('muthialpet', 'Muthialpet'),
        ('rajbhavan', 'Raj Bhavan'),
        ('oupalam', 'Oupalam'),
        ('orleampeth', 'Orleapeth'),
        ('nellithope', 'Nellithope'),
        ('mudaliarpet', 'Mudaliarpet'),
        ('ariankuppam', 'Ariankuppam'),
        ('manavely', 'Manavely'),
        ('embalam', 'Embalam'),
        ('nettapakkam', 'Nettapakkam'),
        ('bahour', 'Bahour'),
        ('nedungadu', 'Nedungadu'),
        ('thirunallar', 'Thirunallar'),
        ('karaikalnorth', 'Karaikal North'),
        ('karaikalsouth', 'Karaikal South'),
        ('neravytrpattinam', 'Neravy T R Pattinam'),
        ('mahe', 'Mahe'),
        ('yanam', 'Yanam'),
        ])
    
    @app.route("/", methods=['GET', 'POST'])
    def hello():
        form = ReusableForm(request.form)
    
        print(form.errors)
        if request.method == 'POST':
            name=request.form['name']
            email=request.form['email']
            signature = Signature(name=name, email=email)
            db.session.add(signature)
            db.session.commit()
            print(name, " ", email)
    
        if form.validate():
            flash('Thanks for signing the campaign ' + name)
    
        return render_template('index.html', form=form)

if __name__ == "__main__":
    app.run()
